*** GINCO - Galaxy INitial COnditions ***

AUTHOR: Davide Fiacconi, Institute of Astronomy, University of Cambridge

EMAIL: fiacconi AT ast.cam.ac.uk

VERSION: 1.0.1

Galaxy INitial COnditions (GINCO) builds the initial conditions (ICs) for near equilibrium
models of galaxies made of: (i) a gaseous disc; (ii) a stellar disc; (iii) a spherical bulge;
and (iv) a spherical dark matter halo. More specifically: (a) the discs are exponential discs
with an isothermal sheet vertical structure (Hernquist 1993), (b) the bulge follows the model
proposed by Hernquist (1990), and (c) the halo is a Navarro, Frenk, and White (1996,1997) halo
truncated at R200. The model is initialised in near equilibrium following Hernquist (1993) and
Springel, Di Matteo, and Hernquist (2005). The main difference is that GINCO uses analytical
approximations of the potential of each component to solve the Jeans equation and to calculate
the first 2 momenta of the velocity distribution function at the position of each particle.
The velocity distribution function is then locally approximated as a Gaussian. GINCO is written
in C and it can run on multiple threads through OpenMP.

** DEPENDENCIES: **

GINCO uses several routines from the Gnu Scientific Libraries (GSL). They are available at the
GSL website, http://www.gnu.org/software/gsl/. Before compiling GINCO, GSL must be compiled and
installed in the system.

** HOW TO BUILD: **

The repository is divided in two directories, "src/" and "model/". The src/ directory contains
the source code with a Makefile for automatic compilation. The user only requires a C compiler
(e.g. gcc) to build the code, once the GSL are installed (see ** DEPENDENCES ** above).

The Makefile contains some compiling-time options (also described in the Makefile):

- -DWITHOMP   : this option enables the OpenMP parallelization.
- -DSAMEDISC  : this option forces the gas disc to have the same radii of the stellar disc.
- -DVERBOSE   : this option enables verbose output on stdout.
- -DOUTPUTLOG : this option produces additional ASCII files about the model (e.g. rotation curves).
- -DASCII     : this option selects the ASCII output format for the ICs.
- -DGADGET2   : this option selects the GADGET2 output format for the ICs.
- -DAREPO     : this option selects the AREPO-compatible GADGET2 output format for the ICs.
- -DTIPSY     : this option selects the standard binary TIPSY/GASOLINE output format for the ICs.

Before compiling the code, enable/disable the desired compiling-time options and set the right
paths for the GSL headers and libraries in the variables GSLINCL and GSLLIBPATH, respectively.
Then, on UNIX systems, one just types

...$ make

and the code should compile. If the compilation is successful, the model/ directory should
contain the "GINCO" executable file. Typing "make clean" would erase all the object files,
the executable and any output file produced by the code.

** HOW TO RUN: **

To run the code, go to the model/ directory. Then, run the code with the following syntax

...$ ./GINCO <parameter_file> [OPTIONS]

GINCO requires a parameter file to run. A template of the parameter file can be created with
the following syntax

...$ ./GINCO newfile

This produces a description of the parameter file on stdout and it creates a new parameter
file "galaxy.param" in the model/ directory that can be modified to initialise the desired
model. Then, the code can be run to produce the ICs as

...$ ./GINCO galaxy.param <integer_number_of_OpenMP_threads>

where the last parameter specifies the number of threads used by the OpenMP. This parameter
is required ONLY IF the code was compiled with the -DWITHOMP option, otherwise is neglected.
According to the chosen file format, the code will produce the files "gas.ascii", "halo.ascii",
"disc.ascii", and "bulge.ascii" if compiled with -DASCII, the file "initcond.dat" if compiled
with -DGADGET2, or the file "initcond.std" if compiled with -DTIPSY. All files contains data
with the following units:

- [LENGTH] = 1 kpc
- [VELOCITY] = 1 km/s
- [MASS] = 2.3262e5 Msol

which implies [TIME] = 0.978 Gyr and G = 1 in code units. The .ascii files all have the same
structure: a sequence of columns containing the mass, the x, y, and z position, and the vx,
vy, and vz velocity, and each row is associated to a particle. The "gas.ascii" file contains
an additional column containing the specific internal energy of each particle in (km/s)^2.

** TIPSY CONVENTIONS **

Since the TIPSY/GASOLINE format has only three particle types, namely gas, dark matter and
stars, we adopt the following convention: the SPH particles are gas particles, while all
the others particles (halo, disc, and bulge) are all dark matter (i.e. simple collisionless
particles). They are dumped in the file with the follwing order: dark matter halo (from
NGas to NGas+NHalo-1), stellar disc (from NGas+NHalo to NGas+NHalo+NDisc-1), and bulge
(from NGas+NHalo+NDisc to NTotal). We chose this solution to distinguish the disc and bulge
particles from the star particles formed during the simulation. Moreover, the TIPSY/GASOLINE
format requires to specify the gas metallicity. By default, the code puts a constant value
equal to the solar metallicity Zsun = 0.019. This can be easily modified directly in the file
src/io_routines/io_tipsy.c.

** CHANGES: **

V1.0.0 : initial repository

V1.0.1 : added GADGET2/AREPO output
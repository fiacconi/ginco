#ifndef __IO_GADGET_H__
#define __IO_GADGET_H__

typedef struct io_gadget_header {
    int npart[6];
    double mass[6];
    double time;
    double redshift;
    int flag_sfr;
    int flag_feedback;
    unsigned int npartTotal[6];
    int flag_cooling;
    int num_files;
    double BoxSize;
    double Omega0;
    double OmegaLambda;
    double HubbleParam;
    int flag_stellarage;
    int flag_metals;
    unsigned int npartTotalHighWord[6];
    int  flag_entropy_instead_u;
    char fill[60];
} GADGET_HEADER;

#endif

#include "../allinc.h"
#include "io_gadget.h"

void init_header(GALMOD *sim, GADGET_HEADER *h) {
    int j;

    for (j=0; j<6; ++j) {
        h->npart[j] = 0;
        h->npartTotal[j] = 0;
        h->npartTotalHighWord[j] = 0;
        h->mass[j] = 0.0;
    }

    if (sim->NGas > 0) {
        h->npart[0] = sim->NGas;
        h->npartTotal[0] = sim->NGas;
        h->mass[0] = sim->mpGas;
    }
    if (sim->NHalo > 0) {
        h->npart[1] = sim->NHalo;
        h->npartTotal[1] = sim->NHalo;
        h->mass[1] = sim->mpHalo;
    }
    if (sim->NDisc > 0) {
        h->npart[2] = sim->NDisc;
        h->npartTotal[2] = sim->NDisc;
        h->mass[2] = sim->mpDisc;
    }
    if (sim->NBulge > 0) {
        h->npart[3] = sim->NBulge;
        h->npartTotal[3] = sim->NBulge;
        h->mass[3] = sim->mpBulge;
    }

    h->time = 0.0;
    h->redshift = 0.0;
    h->flag_sfr = 0;
    h->flag_feedback = 0;
    h->flag_cooling = 0;
    h->num_files = 1;
    h->BoxSize = 0.0;
#ifdef AREPO
    h->BoxSize = sim->box_size;
#endif
    h->Omega0 = 0.0;
    h->OmegaLambda = 0.0;
    h->HubbleParam = 0.0;
    h->flag_metals = 0;
    h->flag_entropy_instead_u = 0;

    if (sim->NTot <= pow(2., 32.)) {
        for(j=0; j<6; ++j) h->npartTotalHighWord[j] = 0;
    }
    else {
        for(j=0; j<6; ++j) h->npartTotalHighWord[j] = h->npartTotal[j]>>31;
    }

    for (j=0; j<60; ++j) h->fill[j] = 0;

    return;
}

void write_ic_gadget(GALMOD *sim) {

    int sizeBox, i;
    FILE *initcond;
    GADGET_HEADER header;
#ifdef VERBOSE
    clock_t t0, t1;
#endif

    /* OPEN FILE AND CHECK */
    initcond = fopen("initcond.dat","wb");
    if (initcond == NULL) {
        fprintf(stderr, "ERROR: cannot open file 'initcond.dat'!\n");
		exit(EXIT_FAILURE);
    }

    /* INITIALISE THE HEADER */
    init_header(sim, &header);

    fprintf(stdout, ">>> WRITING GADGET2 INITIAL CONDITIONS\n");

#ifdef VERBOSE
    t0 = clock();
#endif

    /* WRITE THE HEADER */
    sizeBox = sizeof(GADGET_HEADER);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	fwrite(&header, sizeof(GADGET_HEADER), 1, initcond);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE POSITIONS */
	sizeBox = 3 * sim->NTot * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
    for (i=0; i<sim->NTot; ++i) {
#ifdef AREPO
        sim->p[i].pos[0] = (float)((double)(sim->p[i].pos[0]) + sim->box_size * 0.5);
        sim->p[i].pos[1] = (float)((double)(sim->p[i].pos[1]) + sim->box_size * 0.5);
        sim->p[i].pos[2] = (float)((double)(sim->p[i].pos[2]) + sim->box_size * 0.5);
#endif
        fwrite(sim->p[i].pos, sizeof(float), 3, initcond);
    }
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE VELOCITIES */
	sizeBox = 3 * sim->NTot * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
    for (i=0; i<sim->NTot; ++i) fwrite(sim->p[i].vel, sizeof(float), 3, initcond);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE IDs */
    sizeBox = sim->NTot * sizeof(unsigned int);
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
#ifdef AREPO
    for (i=1; i<=sim->NTot; ++i) fwrite(&(i), sizeof(unsigned int), 1, initcond);
#else
    for (i=0; i<sim->NTot; ++i) fwrite(&(i), sizeof(unsigned int), 1, initcond);
#endif
    fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

    /* WRITE SPH INTERNAL ENERGY */
	if(sim->NGas > 0) {
		sizeBox = sim->NGas * sizeof(float);
        fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
		for(i=0; i<sim->NGas; ++i) fwrite(&(sim->p[i].internal_energy), sizeof(float), 1, initcond);
		fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	}

    /* CLOSE FILE */
    fclose(initcond);

#ifdef VERBOSE
    t1 = clock();
    fprintf(stdout, "--- ICs WRITTEN IN %.6lf s\n\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif

    return;
}

#include "../allinc.h"
#include "io_tipsy.h"

void init_header(GALMOD *sim, TIPSY_HEADER *h) {
    h->time = 0.0;
    h->nbodies = sim->NTot;
    h->ndim = 3;
    h->nsph = sim->NGas;
    h->ndark = sim->NCol;
    h->nstar = 0;
    return;
}

int write_header(XDR *xdr, TIPSY_HEADER *h) {
    int pad = 0;

    if(xdr_double(xdr, &(h->time)) != 1) return 0;
    if(xdr_int(xdr, &(h->nbodies)) != 1) return 0;
    if(xdr_int(xdr, &(h->ndim)) != 1) return 0;
    if(xdr_int(xdr, &(h->nsph)) != 1) return 0;
    if(xdr_int(xdr, &(h->ndark)) != 1) return 0;
    if(xdr_int(xdr, &(h->nstar)) != 1) return 0;
    if(xdr_int(xdr, &pad) != 1) return 0;
    return 1;
}

void write_gas(XDR *xdr, TIPSY_GAS *p) {
    xdr_vector(xdr, (char *)p, (sizeof(TIPSY_GAS)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
    return;
}

void write_dark(XDR *xdr, TIPSY_DARK *p) {
    xdr_vector(xdr, (char *)p, (sizeof(TIPSY_DARK)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
    return;
}

void write_ic_tipsy(GALMOD *sim) {

    int i, j;
    XDR xdr;
    FILE *f;
    TIPSY_HEADER h;
    TIPSY_GAS gp;
    TIPSY_DARK dp;

#ifdef VERBOSE
    clock_t t0, t1;
#endif

    fprintf(stdout, ">>> WRITING TIPSY STD INITIAL CONDITIONS\n");

#ifdef VERBOSE
    t0 = clock();
#endif

    /* OPEN FILE AND INITIALISE */
    f = fopen("initcond.std", "wb");
    xdrstdio_create(&xdr, f, XDR_ENCODE);

    /* INITIALISE AND WRITE THE HEADER */
    init_header(sim, &h);
    if (!write_header(&xdr, &h)) {
        fprintf(stderr, "ERROR: cannot write the header of the file 'initcond.std'!\n");
        exit(EXIT_FAILURE);
    }

    /* FIRST LOOP: GAS */
    for (i=0; i<sim->NGas; ++i) {
        gp.mass = sim->mpGas;
        for (j=0; j<3; ++j) {
            gp.pos[j] = sim->p[i].pos[j];
            gp.vel[j] = sim->p[i].vel[j];
        }
        gp.rho = gp.phi = 0.0;
        gp.temp = 1.0e10 * sim->p[i].internal_energy * (GAMMA - 1.) * 0.59 * PROTONMASS / BOLTZMANN;
        gp.hsmooth = sim->gas_soft;
        gp.metals = 0.019;
        write_gas(&xdr, &gp);
    }

#ifdef VERBOSE
    fprintf(stdout, "--- GAS PARTICLES SET TO SOLAR METALLICITY = 0.019\n");
#endif

    /* SECOND LOOP: HALO */
    for (i=sim->NGas; i<(sim->NGas + sim->NHalo); ++i) {
        dp.mass = sim->mpHalo;
        for (j=0; j<3; ++j) {
            dp.pos[j] = sim->p[i].pos[j];
            dp.vel[j] = sim->p[i].vel[j];
        }
        dp.phi = 0.0;
        dp.eps = sim->halo_soft;
        write_dark(&xdr, &dp);
    }

    /* THIRD LOOP: DISC */
    for (i=sim->NGas + sim->NHalo; i<(sim->NGas + sim->NHalo + sim->NDisc); ++i) {
        dp.mass = sim->mpDisc;
        for (j=0; j<3; ++j) {
            dp.pos[j] = sim->p[i].pos[j];
            dp.vel[j] = sim->p[i].vel[j];
        }
        dp.phi = 0.0;
        dp.eps = sim->disc_soft;
        write_dark(&xdr, &dp);
    }

    /* FOURTH LOOP: BULGE */
    for (i=sim->NGas + sim->NHalo + sim->NDisc; i<sim->NTot; ++i) {
        dp.mass = sim->mpBulge;
        for (j=0; j<3; ++j) {
            dp.pos[j] = sim->p[i].pos[j];
            dp.vel[j] = sim->p[i].vel[j];
        }
        dp.phi = 0.0;
        dp.eps = sim->bulge_soft;
        write_dark(&xdr, &dp);
    }

#ifdef VERBOSE
    t1 = clock();
    fprintf(stdout, "--- ICs WRITTEN IN %.6lf s\n\n", (double)(t1-t0)/CLOCKS_PER_SEC);
#endif

    /* CLOSE FILE AND FINALISE */
    xdr_destroy(&xdr);
    fclose(f);

    return;
}

#include "../allinc.h"

void write_ic_ascii(GALMOD *sim) {

    int i;
    FILE *f;

#ifdef VERBOSE
    clock_t t0, t1, t2;
#endif

    fprintf(stdout, ">>> WRITING ASCII INITIAL CONDITIONS\n");

#ifdef VERBOSE
    t0 = clock();
    t1 = t0;
#endif

    /* WRITE THE GAS */
    f = fopen("gas.ascii","wb");
    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open file 'gas.ascii'!\n");
		exit(EXIT_FAILURE);
    }
    fprintf(f, "#m\tx\ty\tz\tvx\tvy\tvz\tu\tn");
    for(i=0; i<sim->NGas; ++i) fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n", sim->mpGas,
                                        sim->p[i].pos[0], sim->p[i].pos[1], sim->p[i].pos[2],
                                        sim->p[i].vel[0], sim->p[i].vel[1], sim->p[i].vel[2],
                                        sim->p[i].internal_energy);
    fclose(f);
#ifdef VERBOSE
    t2 = clock();
    fprintf(stdout, "--- GAS WRITTEN IN %.6lf s\n\n", (double)(t2-t1)/CLOCKS_PER_SEC);
    t1 = t2;
#endif

    /* WRITE THE HALO */
    f = fopen("halo.ascii","wb");
    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open file 'halo.ascii'!\n");
        exit(EXIT_FAILURE);
    }
    fprintf(f, "#m\tx\ty\tz\tvx\tvy\tvz\tn");
    for(i=sim->NGas; i<sim->NGas+sim->NHalo; ++i) fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\n", sim->mpHalo,
                                    sim->p[i].pos[0], sim->p[i].pos[1], sim->p[i].pos[2],
                                    sim->p[i].vel[0], sim->p[i].vel[1], sim->p[i].vel[2]);
    fclose(f);
#ifdef VERBOSE
    t2 = clock();
    fprintf(stdout, "--- HALO WRITTEN IN %.6lf s\n\n", (double)(t2-t1)/CLOCKS_PER_SEC);
    t1 = t2;
#endif

    /* WRITE THE DISC */
    f = fopen("disc.ascii","wb");
    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open file 'disc.ascii'!\n");
        exit(EXIT_FAILURE);
    }
    fprintf(f, "#m\tx\ty\tz\tvx\tvy\tvz\tn");
    for(i=sim->NGas+sim->NHalo; i<sim->NGas+sim->NHalo+sim->NDisc; ++i) fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\n", sim->mpDisc,
                                sim->p[i].pos[0], sim->p[i].pos[1], sim->p[i].pos[2],
                                sim->p[i].vel[0], sim->p[i].vel[1], sim->p[i].vel[2]);
    fclose(f);
#ifdef VERBOSE
    t2 = clock();
    fprintf(stdout, "--- DISC WRITTEN IN %.6lf s\n\n", (double)(t2-t1)/CLOCKS_PER_SEC);
    t1 = t2;
#endif

    /* WRITE THE BULGE */
    f = fopen("bulge.ascii","wb");
    if (f == NULL) {
        fprintf(stderr, "ERROR: cannot open file 'bulge.ascii'!\n");
        exit(EXIT_FAILURE);
    }
    fprintf(f, "#m\tx\ty\tz\tvx\tvy\tvz\tn");
    for(i=sim->NGas+sim->NHalo+sim->NDisc; i<sim->NTot; ++i) fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\t%e\n", sim->mpBulge,
                            sim->p[i].pos[0], sim->p[i].pos[1], sim->p[i].pos[2],
                            sim->p[i].vel[0], sim->p[i].vel[1], sim->p[i].vel[2]);
    fclose(f);
#ifdef VERBOSE
    t2 = clock();
    fprintf(stdout, "--- BULGE WRITTEN IN %.6lf s\n\n", (double)(t2-t1)/CLOCKS_PER_SEC);
    fprintf(stdout, "--- ICs WRITTEN IN %.6lf s\n\n", (double)(t2-t0)/CLOCKS_PER_SEC);
#endif

    return;
}

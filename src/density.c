#include "allinc.h"

double exponential_disc_density(double r, double z, double M, double rd, double zd) {
	return M * exp(-r/rd) / (4. * PI * rd * rd * zd * cosh(z/zd) * cosh(z/zd));
}

double exponential_disc_surface_density(double r, double M, double rd) {
	return M * exp(-r/rd) / (4. * PI * rd * rd);
}

double bulge_density(double r, double M, double a) {
	return M * a / (2. * PI * r * pow(a + r, 3));
}

double halo_density(double r, double M, double rs, double c) {
	double dc, rhoc;
	dc = 200.0 / 3.0 * pow(c, 3) / (log(1. + c) - c / (1. + c));
	rhoc = M / (200. * 4. * PI * pow(c * rs, 3) / 3.0);
	return rhoc * dc / ((r/rs) * pow(1. + r/rs, 2));
}

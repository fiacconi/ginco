#include "allinc.h"

void read_parameterfile(char *filename, GALMOD *sim) {

	char buffer[4*MAXCHAR], *parameter, *value;
	FILE *parameter_file;

	/* OPEN FILE */
	parameter_file = fopen(filename,"r");

	/* CHECK THAT FILE HAS BEEN OPENED CORRECTLY */
	if (parameter_file == NULL) {
		fprintf(stderr, "ERROR: cannot open file '%s'!\n", filename);
		exit(EXIT_FAILURE);
	}

	/* INITIALISE VARIABLES */
	sim->NTot = sim->NCol = sim->NGas = sim->NHalo = sim->NDisc = sim->NBulge = 0;
	sim->MGas = sim->MHalo = sim->MDisc = sim->MBulge = 0.0;
	sim->mpGas = sim->mpHalo = sim->mpDisc = sim->mpBulge = 0.0;
	sim->R_gas_out = sim->h_gas = sim->z0_gas = sim->turb_gas = 0.0;
	sim->v200 = sim->r200 = sim->a_halo = sim->c_halo = 0.0;
	sim->R_disc_out = sim->h_disc = sim->z0_disc = 0.0;
	sim->R_bulge_out = sim->a_bulge = 0.0;
	sim->box_size = 0.0;

	/* LOOP OVER THE LINES OF THE PARAMETER FILE */
	while (!feof(parameter_file)) {

		/* READ AN ENTIRE LINE */
        fgets(buffer, MAXCHAR, parameter_file);

		/* SKIP COMMENTS OR EMPTY LINES */
        if (buffer[0] == '#' || buffer[0] == '%' || !strcmp(buffer, "\n")) continue;

		/* READ PARAMETER NAME FIRST AND THEN PARAMETER VALUE */
        parameter = strtok(buffer, " \t");
        value = strtok(NULL, " \t");

        if (!strcmp(parameter, "RandomSeed"))
			sim->random_seed = (unsigned long int)atoi(value);
        else if (!strcmp(parameter, "NGasPart"))
			sim->NGas = (unsigned int)atoi(value);
        else if (!strcmp(parameter, "GasMass"))
			sim->MGas = (double)atof(value);
        else if (!strcmp(parameter, "GasMaxRad"))
			sim->R_gas_out = (double)atof(value);
		else if (!strcmp(parameter, "GasRadScale"))
			sim->h_gas = (double)atof(value);
		else if (!strcmp(parameter, "GasVertScale"))
			sim->z0_gas = (double)atof(value);
		else if (!strcmp(parameter, "GasTurbulence"))
			sim->turb_gas = (double)atof(value);
		else if (!strcmp(parameter, "NHaloPart"))
			sim->NHalo = (unsigned int)atoi(value);
		else if (!strcmp(parameter, "HaloVel200"))
			sim->v200 = (double)atof(value);
		else if (!strcmp(parameter, "HaloConcPar"))
			sim->c_halo = (double)atof(value);
		else if (!strcmp(parameter, "NDiscPart"))
			sim->NDisc = (unsigned int)atoi(value);
		else if (!strcmp(parameter, "DiscMass"))
			sim->MDisc = (double)atof(value);
		else if (!strcmp(parameter, "DiscMaxRad"))
			sim->R_disc_out = (double)atof(value);
		else if (!strcmp(parameter, "DiscRadScale"))
			sim->h_disc = (double)atof(value);
		else if (!strcmp(parameter, "DiscVertScale"))
			sim->z0_disc = (double)atof(value);
		else if (!strcmp(parameter, "NBulgePart"))
			sim->NBulge = (unsigned int)atoi(value);
		else if (!strcmp(parameter, "BulgeMass"))
			sim->MBulge = (double)atof(value);
		else if (!strcmp(parameter, "BulgeMaxRad"))
			sim->R_bulge_out = (double)atof(value);
		else if (!strcmp(parameter, "BulgeRadScale"))
			sim->a_bulge = (double)atof(value);
#ifdef TIPSY
		else if (!strcmp(parameter, "GasSoftening"))
			sim->gas_soft = (double)atof(value);
		else if (!strcmp(parameter, "HaloSoftening"))
			sim->halo_soft = (double)atof(value);
		else if (!strcmp(parameter, "DiscSoftening"))
			sim->disc_soft = (double)atof(value);
		else if (!strcmp(parameter, "BulgeSoftening"))
			sim->bulge_soft = (double)atof(value);
#endif
#ifdef AREPO
		else if (!strcmp(parameter, "BoxSize"))
			sim->box_size = (double)atof(value);
#endif
		else fprintf(stderr, "Parameter '%s' not understood, skipping.\n", parameter);
    }

	/* CLOSE FILE */
	fclose(parameter_file);

	/* CHECK THE COMPONENTS */
	if (sim->NGas == 0) {
		sim->MGas = 0.0;
		sim->R_gas_out = 0.0;
		sim->h_gas = 0.0;
		sim->z0_gas = 0.0;
	}
	if (sim->NHalo == 0) {
		sim->MHalo = 0.0;
		sim->v200 = 0.0;
		sim->c_halo = 0.0;
	}
	if (sim->NDisc == 0) {
		sim->MDisc = 0.0;
		sim->R_disc_out = 0.0;
		sim->h_disc = 0.0;
		sim->z0_disc = 0.0;
	}
	if (sim->NBulge == 0) {
		sim->MBulge = 0.0;
		sim->R_bulge_out = 0.0;
		sim->a_bulge = 0.0;
	}
	return;
}

void write_parameterfile_template() {

	FILE *param_file_template;

	/* OPEN FILE */
	param_file_template = fopen("galaxy.param","w");

	/* CHECK THAT FILE HAS BEEN OPENED CORRECTLY */
	if (param_file_template == NULL) {
		fprintf(stderr, "ERROR: cannot open file 'galaxy.param'!\n");
		exit(EXIT_FAILURE);
	}

	fprintf(param_file_template,"RandomSeed ***\n");

	fprintf(param_file_template,"\n");

	fprintf(param_file_template,"NGasPart ***\n");
	fprintf(param_file_template,"GasMass ***\n");
	fprintf(param_file_template,"GasMaxRad ***\n");
	fprintf(param_file_template,"GasRadScale ***\n");
	fprintf(param_file_template,"GasVertScale ***\n");
	fprintf(param_file_template,"GasTemp ***\n");

	fprintf(param_file_template,"\n");

	fprintf(param_file_template,"NHaloPart ***\n");
	fprintf(param_file_template,"HaloVel200 ***\n");
	fprintf(param_file_template,"HaloConcPar ***\n");

	fprintf(param_file_template,"\n");

	fprintf(param_file_template,"NDiscPart ***\n");
	fprintf(param_file_template,"DiscMass ***\n");
	fprintf(param_file_template,"DiscMaxRad ***\n");
	fprintf(param_file_template,"DiscRadScale ***\n");
	fprintf(param_file_template,"DiscVertScale ***\n");

	fprintf(param_file_template,"\n");

	fprintf(param_file_template,"NBulgePart ***\n");
	fprintf(param_file_template,"BulgeMass ***\n");
	fprintf(param_file_template,"BulgeMaxRad ***\n");
	fprintf(param_file_template,"BulgeRadScale ***\n");

#ifdef TIPSY
	fprintf(param_file_template,"GasSoftening ***\n");
	fprintf(param_file_template,"HaloSoftening ***\n");
	fprintf(param_file_template,"DiscSoftening ***\n");
	fprintf(param_file_template,"BulgeSoftening ***\n");
#endif

#ifdef AREPO
	fprintf(param_file_template,"BoxSize ***\n");
#endif

	fprintf(param_file_template,"\n\n\n");

	/* CLOSE FILE */
	fclose(param_file_template);

	fprintf(stdout, "\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|\n");
	fprintf(stdout,   "|  >>> The file 'galaxy.param' was written succesfully! <<<  |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| Replace the characters * with the chosen values.           |\n");
	fprintf(stdout,   "| If you do not want to use a component of the model (e.g.   |\n");
	fprintf(stdout,   "| the gas disk), simply set the particle number to 0.        |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| IMPORTANT: Masses MUST be written in units of 1e10 solar   |\n");
	fprintf(stdout,   "| masses, lengths in units of 1 kpc, velocities in units of  |\n");
	fprintf(stdout,   "| kilometers per second, and tempertures in Kelvin degree.   |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| Parameters are:                                            |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| RandomSeed = POSITIVE seed for the random generator        |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| # EXPONENTIAL GAS DISC SECTION                             |\n");
	fprintf(stdout,   "| NGasPart = Number of gas particles                         |\n");
	fprintf(stdout,   "| GasMass = Total mass of the gas disc                       |\n");
	fprintf(stdout,   "| GasMaxRad = Max radius of the gas disc                     |\n");
	fprintf(stdout,   "| GasRadScale = Radial scale of the gas disc                 |\n");
	fprintf(stdout,   "| GasVertScale = Vertical scale of the gas disc              |\n");
	fprintf(stdout,   "| GasTurbulence = constant velocity dispersion of the gas    |\n");
	fprintf(stdout,   "|                 (THIS CAN BE SET TO 0 FOR LAMINAR FLOW)    |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| # NFW DARK MATTER HALO SECTION                             |\n");
	fprintf(stdout,   "| NHaloPart = Number of dark matter halo particles           |\n");
	fprintf(stdout,   "| HaloVel200 = Circular velocity of the halo at R200         |\n");
	fprintf(stdout,   "| HaloConcPar = Concentration parameter of the halo          |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| # EXPONENTIAL STELLAR DISC SECTION                         |\n");
	fprintf(stdout,   "| NDiscPart = Number of stellar disc particles               |\n");
	fprintf(stdout,   "| DiscMass = Total mass of the stellar disc                  |\n");
	fprintf(stdout,   "| DiscMaxRad = Max radius of the stellar disc                |\n");
	fprintf(stdout,   "| DiscRadScale = Radial scale of the stellar disc            |\n");
	fprintf(stdout,   "| DiscVertScale = Vertical scale of the stellar disc         |\n");
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "| # HERNQUIST STELLAR BULGE SECTION                          |\n");
	fprintf(stdout,   "| NBulgePart = Number of stellar bulge particles             |\n");
	fprintf(stdout,   "| BulgeMass = Total mass of the stellar bulge                |\n");
	fprintf(stdout,   "| BulgeMaxRad = Max radius of the stellar bulge              |\n");
	fprintf(stdout,   "| BulgeRadScale = Scale length of the stellar bulge          |\n");
#ifdef SAMEDISC
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "|        ! WARNING - CODE COMPILED WITH -DSAMEDISC !         |\n");
	fprintf(stdout,   "| The gasseous and stellar disc geometrical parameters MUST  |\n");
	fprintf(stdout,   "| BE THE SAME! Otherwise, GINCO will set both of them to the |\n");
	fprintf(stdout,   "| stellar disc values.                                       |\n");
#endif
#ifdef TIPSY
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "|          ! WARNING - CODE COMPILED WITH -DTIPSY !          |\n");
	fprintf(stdout,   "| GASOLINE/TIPSY files require the softening of each         |\n");
	fprintf(stdout,   "| particle to be specified in the initial condition file.    |\n");
	fprintf(stdout,   "| This can be done through the following parameters:         |\n");
	fprintf(stdout,   "| GasSoftening = gravitational softening of the gas          |\n");
	fprintf(stdout,   "| HaloSoftening = gravitational softening of the halo        |\n");
	fprintf(stdout,   "| DiscSoftening = gravitational softening of the disc        |\n");
	fprintf(stdout,   "| BulgeSoftening = gravitational softening of the bulge      |\n");
#endif
#ifdef AREPO
	fprintf(stdout,   "|                                                            |\n");
	fprintf(stdout,   "|          ! WARNING - CODE COMPILED WITH -DAREPO !          |\n");
	fprintf(stdout,   "| AREPO/GADGET2 files require the box size to be stored in   |\n");
	fprintf(stdout,   "| the initial conditions' file header.                       |\n");
	fprintf(stdout,   "| This can be done through the following parameter:          |\n");
	fprintf(stdout,   "| BoxSize = Size of the computational box                    |\n");
#endif
	fprintf(stdout,   "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|\n\n");
	return;
}

void write_snapshot(GALMOD *sim) {

	fprintf(stdout, "\n");

#ifdef ASCII
	write_ic_ascii(sim);
#endif
#if defined(GADGET2) || defined(AREPO)
	write_ic_gadget(sim);
#endif
#ifdef TIPSY
	write_ic_tipsy(sim);
#endif

	return;
}

#include "allinc.h"

double linear_interpolation(double x, double *xx, double *yy) {

    int i = (int)((log10(x) + 4.) * (2. * MAXCHAR - 1) / 4.);
    double result;

    if (i < 0) {
        result = (yy[0] - yy[1]) / (xx[0] - xx[1]) * (x - xx[0]) + yy[0];
    }
    else {
        result = (yy[i] - yy[i+1]) / (xx[i] - xx[i+1]) * (x - xx[i]) + yy[i];
    }

    return result;
}

void make_positions(GALMOD *sim) {

    int i;

#ifdef VERBOSE
    #ifdef WITHOMP
        double t1, t0 = omp_get_wtime();
    #else
        clock_t t1, t0 = clock();
    #endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(i)
    {
        int nid = omp_get_thread_num();
        gsl_rng *local_rng;
        local_rng = sim->rng[nid];

#else
        gsl_rng *local_rng;
        local_rng = sim->rng;
#endif
        int start, end;
        double phi, theta, R, r;

        /* FIRST LOOP: GAS PARTICLES */
        start = 0;
        end = sim->NGas;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            phi = azimuthal_angle(local_rng);
            R = sim->h_gas * exponential_disc_radius(sim->R_gas_out, sim->h_gas, local_rng);
            sim->p[i].pos[2] = (float)(sim->z0_gas * exponential_disc_vertical(local_rng));
            sim->p[i].pos[0] = (float)(R * cos(phi));
            sim->p[i].pos[1] = (float)(R * sin(phi));
        }

        /* SECOND LOOP: HALO PARTICLES */
        start = sim->NGas;
        end = sim->NGas+sim->NHalo;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            phi = azimuthal_angle(local_rng);
            theta = colatitude_angle(local_rng);
            r = sim->a_halo * halo_sferical_radius(sim->c_halo, local_rng);
            sim->p[i].pos[0] = (float)(r * sin(theta) * cos(phi));
            sim->p[i].pos[1] = (float)(r * sin(theta) * sin(phi));
            sim->p[i].pos[2] = (float)(r * cos(theta));
        }

        /* THIRD LOOP: DISC PARTICLES */
        start = sim->NGas+sim->NHalo;
        end = sim->NGas+sim->NHalo+sim->NDisc;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            phi = azimuthal_angle(local_rng);
            R = sim->h_disc * exponential_disc_radius(sim->R_disc_out, sim->h_disc, local_rng);
            sim->p[i].pos[2] = (float)(sim->z0_disc * exponential_disc_vertical(local_rng));
            sim->p[i].pos[0] = (float)(R * cos(phi));
            sim->p[i].pos[1] = (float)(R * sin(phi));
        }

        /* FOURTH LOOP: BULGE PARTICLES */
        start = sim->NGas+sim->NHalo+sim->NDisc;
        end = sim->NTot;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            phi = azimuthal_angle(local_rng);
            theta = colatitude_angle(local_rng);
            r = sim->a_bulge * bulge_sferical_radius(sim->a_bulge, sim->R_bulge_out, local_rng);
            sim->p[i].pos[0] = (float)(r * sin(theta) * cos(phi));
            sim->p[i].pos[1] = (float)(r * sin(theta) * sin(phi));
            sim->p[i].pos[2] = (float)(r * cos(theta));
        }
#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
    #ifdef WITHOMP
        t1 = omp_get_wtime();
        fprintf(stdout, ">>> PARTICLE POSITIONS INITIALISED IN %.6lf s\n", t1-t0);
    #else
        t1 = clock();
        fprintf(stdout, ">>> PARTICLE POSITIONS INITIALISED IN %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
    #endif
#endif

    return;
}

void make_velocities(GALMOD *sim) {

    int i;

#ifdef VERBOSE
    #ifdef WITHOMP
        double t1, t0 = omp_get_wtime();
    #else
        clock_t t1, t0 = clock();
    #endif
#endif

#ifdef WITHOMP
    #pragma omp parallel default(shared) private(i)
    {
        int nid = omp_get_thread_num();
        gsl_rng *local_rng;
        local_rng = sim->rng[nid];
#else
        int start, end;
        gsl_rng *local_rng;
        local_rng = sim->rng;
#endif

        int start, end;
        double R, r, z, theta, phi, cs, vc2, vphi, vesc, sigmar, sigmaz, sigma_norm, omega2, kappa2, sigmaphi, vr;
        double r_dimless[2*MAXCHAR], sigma_halo[2*MAXCHAR], sigma_bulge[2*MAXCHAR];

        /* INIT NORMALIZATION FOR RADIAL VELOCITY DISPERSION OF THE STELLAR DISC */
        sigma_norm = 3.36 * G * exponential_disc_surface_density(2.5*sim->h_disc, sim->MDisc, sim->h_disc) * Q_toomre /
            exponential_disc_vertical_dispersion(2.5*sim->h_disc, sim->MGas, sim->h_gas, sim->z0_gas,
                                                                sim->MDisc, sim->h_disc, sim->z0_disc) /
            sqrt(exponential_disc_kappa2_ang(2.5*sim->h_disc, 0.0, sim->MGas, sim->h_gas) +
                exponential_disc_kappa2_ang(2.5*sim->h_disc, 0.0, sim->MDisc, sim->h_disc) +
                bulge_kappa2_ang(2.5*sim->h_disc, 0.0, sim->MBulge, sim->a_bulge) +
                halo_kappa2_ang(2.5*sim->h_disc, 0.0, sim->v200, sim->a_halo, sim->c_halo));

        /* INITIALISE THE VELOCITIES LOCALLY FOR SUBSEQUENT INTERPOLATIONS */
        for (i=0; i<2*MAXCHAR; ++i) {
            r_dimless[i] = pow(10., -4. + 4. * i / (2. * MAXCHAR - 1.));
            sigma_halo[i] = halo_dispersion_velocity(r_dimless[i] * sim->r200,
                                                        sim->MGas,
            									        sim->h_gas,
            									        sim->v200,
            									        sim->a_halo,
            									        sim->c_halo,
            								            sim->MDisc,
            									        sim->h_disc,
            									        sim->MBulge,
            									        sim->a_bulge);
            sigma_bulge[i] = bulge_dispersion_velocity(r_dimless[i] * sim->R_bulge_out,
                                                        sim->MGas,
            									        sim->h_gas,
            									        sim->v200,
            									        sim->a_halo,
            									        sim->c_halo,
            								            sim->MDisc,
            									        sim->h_disc,
            									        sim->MBulge,
            									        sim->a_bulge);
        }

        /* FIRST LOOP: GAS PARTICLES */
        start = 0;
        end = sim->NGas;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            R = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) );
            z = sim->p[i].pos[2];
            cs = gas_soundspeed(R, z,
                                sim->MGas,
                                sim->h_gas,
                                sim->z0_gas,
                                sim->MDisc,
                                sim->h_disc,
                                sim->z0_disc);

            if (cs < 10.0) cs = 10.0;

            vc2 = exponential_disc_vc_squared(R, z, sim->MGas, sim->h_gas) +
                    exponential_disc_vc_squared(R, z, sim->MDisc, sim->h_disc) +
                    bulge_vc_squared(R, z, sim->MBulge, sim->a_bulge) +
                    halo_vc_squared(R, z, sim->v200, sim->a_halo, sim->c_halo);

            vphi = sqrt(vc2 - cs * cs * R / sim->h_gas);

            if (isnan(vphi)) vphi = sqrt(vc2);

            sim->p[i].vel[0] = (float)(-vphi * sim->p[i].pos[1] / R);
            sim->p[i].vel[1] = (float)(vphi * sim->p[i].pos[0] / R);
            sim->p[i].vel[2] = 0.0;
            sim->p[i].internal_energy = (float)(cs * cs / (GAMMA - 1.) / GAMMA);
            if (sim->turb_gas > 0.0) {
                sim->p[i].vel[0] += (float)gsl_ran_gaussian(local_rng, sim->turb_gas / SQRT3);
                sim->p[i].vel[1] += (float)gsl_ran_gaussian(local_rng, sim->turb_gas / SQRT3);
                sim->p[i].vel[2] += (float)gsl_ran_gaussian(local_rng, sim->turb_gas / SQRT3);
            }
        }

        /* SECOND LOOP: HALO PARTICLES */
        start = sim->NGas;
        end = sim->NGas+sim->NHalo;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            R = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) );
            r = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) + pow(sim->p[i].pos[2], 2) );
            z = sim->p[i].pos[2];

            vesc = sqrt(2. * fabs(halo_potential(R, z, sim->v200, sim->a_halo, sim->c_halo) +
                                    bulge_potential(R, z, sim->MBulge, sim->a_bulge) +
                                    exponential_disc_potential(R, z, sim->MGas, sim->h_gas, sim->z0_gas) +
                                    exponential_disc_potential(R, z, sim->MDisc, sim->h_disc, sim->z0_disc)) );

            sigmar = linear_interpolation(r/sim->r200, r_dimless, sigma_halo);

            vphi = maxwellian_velocity_distribution(local_rng, sigmar, vesc);
            theta = colatitude_angle(local_rng);
            phi = azimuthal_angle(local_rng);

            sim->p[i].vel[0] = (float)(vphi * sin(theta) * cos(phi));
            sim->p[i].vel[1] = (float)(vphi * sin(theta) * sin(phi));
            sim->p[i].vel[2] = (float)(vphi * cos(theta));
        }

        /* THIRD LOOP: DISC PARTICLES */
        start = sim->NGas+sim->NHalo;
        end = sim->NGas+sim->NHalo+sim->NDisc;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif

            R = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) );
            z = sim->p[i].pos[2];
            sigmaz = exponential_disc_vertical_dispersion(R, sim->MGas, sim->h_gas, sim->z0_gas, sim->MDisc, sim->h_disc, sim->z0_disc);
            sigmar = sigma_norm * sigmaz;

            omega2 = exponential_disc_omega2_ang(R, z, sim->MGas, sim->h_gas) +
                exponential_disc_omega2_ang(R, z, sim->MDisc, sim->h_disc) +
                bulge_omega2_ang(R, z, sim->MBulge, sim->a_bulge) +
                halo_omega2_ang(R, z, sim->v200, sim->a_halo, sim->c_halo);

            kappa2 = exponential_disc_kappa2_ang(R, z, sim->MGas, sim->h_gas) +
                exponential_disc_kappa2_ang(R, z, sim->MDisc, sim->h_disc) +
                bulge_kappa2_ang(R, z, sim->MBulge, sim->a_bulge) +
                halo_kappa2_ang(R, z, sim->v200, sim->a_halo, sim->c_halo);

            sigmaphi = sqrt(0.5 * omega2 / kappa2) * sigmar;

            vc2 = exponential_disc_vc_squared(R, z, sim->MGas, sim->h_gas) +
                exponential_disc_vc_squared(R, z, sim->MDisc, sim->h_disc) +
                bulge_vc_squared(R, z, sim->MBulge, sim->a_bulge) +
                halo_vc_squared(R, z, sim->v200, sim->a_halo, sim->c_halo);

            vphi = sqrt(vc2 + sigmar * (1. - 2. * R / sim->h_disc) - sigmaphi);
            if (isnan(vphi)) vphi = sqrt(vc2);

            vr = gsl_ran_gaussian(local_rng, sigmar);
            vphi += gsl_ran_gaussian(local_rng, sigmaphi);

            sim->p[i].vel[0] = (float)(vr * sim->p[i].pos[0] / R - vphi * sim->p[i].pos[1] / R);
            sim->p[i].vel[1] = (float)(vr * sim->p[i].pos[1] / R + vphi * sim->p[i].pos[0] / R);
            sim->p[i].vel[2] = (float)gsl_ran_gaussian(local_rng, sigmaz);
        }

        /* FOURTH LOOP: BULGE PARTICLES */
        start = sim->NGas+sim->NHalo+sim->NDisc;
        end = sim->NTot;
#ifdef WITHOMP
        #pragma omp for schedule(static)
        for (i=start; i<end; ++i) {
#else
        for (i=start; i<end; ++i) {
#endif
            R = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) );
            r = sqrt( pow(sim->p[i].pos[0], 2) + pow(sim->p[i].pos[1], 2) + pow(sim->p[i].pos[2], 2) );
            z = sim->p[i].pos[2];

            vesc = sqrt(2. * fabs(halo_potential(R, z, sim->v200, sim->a_halo, sim->c_halo) +
                                    bulge_potential(R, z, sim->MBulge, sim->a_bulge) +
                                    exponential_disc_potential(R, z, sim->MGas, sim->h_gas, sim->z0_gas) +
                                    exponential_disc_potential(R, z, sim->MDisc, sim->h_disc, sim->z0_disc)) );

            sigmar = linear_interpolation(r/sim->R_bulge_out, r_dimless, sigma_bulge);

            vphi = maxwellian_velocity_distribution(local_rng, sigmar, vesc);
            theta = colatitude_angle(local_rng);
            phi = azimuthal_angle(local_rng);

            sim->p[i].vel[0] = (float)(vphi * sin(theta) * cos(phi));
            sim->p[i].vel[1] = (float)(vphi * sin(theta) * sin(phi));
            sim->p[i].vel[2] = (float)(vphi * cos(theta));
        }

#ifdef OUTPUTLOG
#ifdef WITHOMP
        #pragma omp master
        {
#endif

#ifdef VERBOSE
    #ifdef WITHOMP
            t1 = omp_get_wtime();
    #else
            t1 = clock();
    #endif
#endif

            FILE *f;
            if ((f = fopen("halo.dat", "w")) == NULL) {
                fprintf(stderr, "ERROR: cannot open file 'halo.dat'!\n");
                exit(EXIT_FAILURE);
            }
            fprintf(f, "#r(kpc)\trho(Msol/kpc^3)\tsigma(km/s)\tVc(km/s)\n");
            for (i=0; i<2*MAXCHAR; ++i) {
                r = r_dimless[i] * sim->r200;
                fprintf(f, "%e\t%e\t%e\t%e\n",
                            r,
                            halo_density(r, pow(sim->v200, 3) / 10. / G, sim->a_halo, sim->c_halo) * mass_in_solar_masses,
                            sigma_halo[i],
                            sqrt(halo_vc_squared(r, 0.0, sim->v200, sim->a_halo, sim->c_halo))
                        );
            }
            fclose(f);

            if ((f = fopen("bulge.dat", "w")) == NULL) {
                fprintf(stderr, "ERROR: cannot open file 'bulge.dat'!\n");
                exit(EXIT_FAILURE);
            }
            fprintf(f, "#r(kpc)\trho(Msol/kpc^3)\tsigma(km/s)\tVc(km/s)\n");
            for (i=0; i<2*MAXCHAR; ++i) {
                r = r_dimless[i] * sim->R_bulge_out;
                fprintf(f, "%e\t%e\t%e\t%e\n",
                            r,
                            bulge_density(r, sim->MBulge, sim->a_bulge) * mass_in_solar_masses,
                            sigma_bulge[i],
                            sqrt(bulge_vc_squared(r, 0.0, sim->MBulge, sim->a_bulge))
                        );
            }
            fclose(f);

            if ((f = fopen("disc.dat", "w")) == NULL) {
                fprintf(stderr, "ERROR: cannot open file 'disc.dat'!\n");
                exit(EXIT_FAILURE);
            }
            fprintf(f, "#r(kpc)\tSigma(Msol/kpc^2)\tsigmaz(km/s)\tsigmar(km/s)\tVphi(km/s)\tQ\n");
            for (i=0; i<2*MAXCHAR; ++i) {
                R = sim->R_disc_out * (0.5 + i) / (2. * MAXCHAR);
                sigmaz = exponential_disc_vertical_dispersion(R, sim->MGas, sim->h_gas, sim->z0_gas, sim->MDisc, sim->h_disc, sim->z0_disc);

                sigmar = sigma_norm * sigmaz;

                omega2 = exponential_disc_omega2_ang(R, 0.0, sim->MGas, sim->h_gas) +
                    exponential_disc_omega2_ang(R, 0.0, sim->MDisc, sim->h_disc) +
                    bulge_omega2_ang(R, 0.0, sim->MBulge, sim->a_bulge) +
                    halo_omega2_ang(R, 0.0, sim->v200, sim->a_halo, sim->c_halo);

                kappa2 = exponential_disc_kappa2_ang(R, 0.0, sim->MGas, sim->h_gas) +
                    exponential_disc_kappa2_ang(R, 0.0, sim->MDisc, sim->h_disc) +
                    bulge_kappa2_ang(R, 0.0, sim->MBulge, sim->a_bulge) +
                    halo_kappa2_ang(R, 0.0, sim->v200, sim->a_halo, sim->c_halo);

                sigmaphi = sqrt(0.5 * omega2 / kappa2) * sigmar;

                vc2 = exponential_disc_vc_squared(R, 0.0, sim->MGas, sim->h_gas) +
                    exponential_disc_vc_squared(R, 0.0, sim->MDisc, sim->h_disc) +
                    bulge_vc_squared(R, 0.0, sim->MBulge, sim->a_bulge) +
                    halo_vc_squared(R, 0.0, sim->v200, sim->a_halo, sim->c_halo);

                vphi = sqrt(vc2 + sigmar * (1. - 2. * R / sim->h_disc) - sigmaphi);
                if (isnan(vphi)) vphi = sqrt(vc2);

                fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\n",
                            R,
                            exponential_disc_surface_density(R, sim->MDisc, sim->h_disc) * mass_in_solar_masses,
                            sigmaz,
                            sigmar,
                            vphi,
                            sqrt(kappa2)*sigmar / (3.36 * G * exponential_disc_surface_density(R, sim->MDisc, sim->h_disc))
                        );
            }
            fclose(f);

            if ((f = fopen("gas.dat", "w")) == NULL) {
                fprintf(stderr, "ERROR: cannot open file 'gas.dat'!\n");
                exit(EXIT_FAILURE);
            }
            fprintf(f, "#r(kpc)\tSigma(Msol/kpc^2)\tcs(km/s)\tT(k)\tVphi(km/s)\tQ\n");
            for (i=0; i<2*MAXCHAR; ++i) {
                R = sim->R_disc_out * (0.5 + i) / (2. * MAXCHAR);

                cs = gas_soundspeed(R, 0.0, sim->MGas, sim->h_gas, sim->z0_gas, sim->MDisc, sim->h_disc, sim->z0_disc);
                if (cs < 10.0) cs = 10.0;

                kappa2 = exponential_disc_kappa2_ang(R, 0.0, sim->MGas, sim->h_gas) +
                    exponential_disc_kappa2_ang(R, 0.0, sim->MDisc, sim->h_disc) +
                    bulge_kappa2_ang(R, 0.0, sim->MBulge, sim->a_bulge) +
                    halo_kappa2_ang(R, 0.0, sim->v200, sim->a_halo, sim->c_halo);

                vc2 = exponential_disc_vc_squared(R, z, sim->MGas, sim->h_gas) +
                        exponential_disc_vc_squared(R, z, sim->MDisc, sim->h_disc) +
                        bulge_vc_squared(R, z, sim->MBulge, sim->a_bulge) +
                        halo_vc_squared(R, z, sim->v200, sim->a_halo, sim->c_halo);

                vphi = sqrt(vc2 - cs * cs * R / sim->h_gas);
                if (isnan(vphi)) vphi = sqrt(vc2);

                fprintf(f, "%e\t%e\t%e\t%e\t%e\t%e\n",
                            R,
                            exponential_disc_surface_density(R, sim->MGas, sim->h_gas) * mass_in_solar_masses,
                            cs,
                            1.0e10 * cs * cs * 0.59 * PROTONMASS / BOLTZMANN / GAMMA,
                            vphi,
                            sqrt(vc2) / R * cs / (PI * G * exponential_disc_surface_density(R, sim->MGas, sim->h_gas))
                        );
            }
            fclose(f);

#ifdef VERBOSE
    #ifdef WITHOMP
            fprintf(stdout, "--- AUXILIARY FILES .dat WRITTEN in %.6lf s\n", omp_get_wtime()-t1);
    #else
            fprintf(stdout, "--- AUXILIARY FILES .dat WRITTEN in %.6lf s\n", (double)(clock()-t1)/CLOCKS_PER_SEC);
    #endif
#endif

#ifdef WITHOMP
        }
#endif
#endif

#ifdef WITHOMP
    }
#endif

#ifdef VERBOSE
    #ifdef WITHOMP
        t1 = omp_get_wtime();
        fprintf(stdout, ">>> PARTICLE VELOCITIES INITIALISED IN %.6lf s\n", t1-t0);
    #else
        t1 = clock();
        fprintf(stdout, ">>> PARTICLE VELOCITIES INITIALISED IN %.6lf s\n", (double)(t1-t0)/CLOCKS_PER_SEC);
    #endif
#endif

    return;
}

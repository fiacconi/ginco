#include "allinc.h"

double bulge_dispersion_velocity(double rad,
									double Mg,
									double Rg,
									double Vh,
									double ah,
									double ch,
									double Md,
									double Rd,
									double Mb,
									double ab) {

	gsl_integration_workspace *Dom;
	gsl_function F;
	double result, error, params[9];

	params[0] = Mg;
	params[1] = Rg;
	params[2] = Vh;
	params[3] = ah;
	params[4] = ch;
	params[5] = Md;
	params[6] = Rd;
	params[7] = Mb;
	params[8] = ab;

	double integrand(double r, void * param)
	{
		double mg = ((double *)param)[0];
		double ag = ((double *)param)[1];
		double vh = ((double *)param)[2];
		double ah = ((double *)param)[3];
		double ch = ((double *)param)[4];
		double md = ((double *)param)[5];
		double ad = ((double *)param)[6];
		double mb = ((double *)param)[7];
		double ab = ((double *)param)[8];

		return bulge_density(r, mb, ab) * ( disc_sph_potential_radgrad(r, mg, ag) +
											disc_sph_potential_radgrad(r, md, ad) +
											halo_potential_radgrad(r, vh, ah, ch) +
											bulge_potential_radgrad(r, mb, ab) );
	}

	F.function = &integrand;
	F.params = (void *)params;
	Dom = gsl_integration_workspace_alloc(500);

	gsl_integration_qagiu(&F, rad, 1.0e-9, 1e-5, 500, Dom, &result, &error);
	gsl_integration_workspace_free(Dom);

	return sqrt(result / bulge_density(rad, Mb, ab));
}

double halo_dispersion_velocity(double rad,
									double Mg,
									double Rg,
									double Vh,
									double ah,
									double ch,
									double Md,
									double Rd,
									double Mb,
									double ab) {

	gsl_integration_workspace *Dom;
	gsl_function F;
	double result, error, params[9];

	params[0] = Mg;
	params[1] = Rg;
	params[2] = Vh;
	params[3] = ah;
	params[4] = ch;
	params[5] = Md;
	params[6] = Rd;
	params[7] = Mb;
	params[8] = ab;

	double integrand(double r, void * param)
	{
		double mg = ((double *)param)[0];
		double ag = ((double *)param)[1];
		double vh = ((double *)param)[2];
		double ah = ((double *)param)[3];
		double ch = ((double *)param)[4];
		double md = ((double *)param)[5];
		double ad = ((double *)param)[6];
		double mb = ((double *)param)[7];
		double ab = ((double *)param)[8];

		return halo_density(r, pow(vh, 3) / 10. / G, ah, ch) * ( disc_sph_potential_radgrad(r, mg, ag) +
											disc_sph_potential_radgrad(r, md, ad) +
											halo_potential_radgrad(r, vh, ah, ch) +
											bulge_potential_radgrad(r, mb, ab) );
	}

	F.function = &integrand;
	F.params = (void *)params;
	Dom = gsl_integration_workspace_alloc(500);

	gsl_integration_qagiu(&F, rad, 1.0e-9, 1e-5, 500, Dom, &result, &error);
	gsl_integration_workspace_free(Dom);

	return sqrt(result / halo_density(rad, pow(Vh, 3) / 10. / G, ah, ch));
}

double maxwellian_velocity_distribution(gsl_rng *rng, double sigma, double vesc) {

	double x_max=vesc / SQRT2 / sigma;
	double yprob = (gsl_sf_erf(x_max) - 2.0 / SQRTPI * x_max * exp(-x_max * x_max)) * gsl_rng_uniform(rng);

	double maxwell(double x) {
		return yprob - (gsl_sf_erf(x) - 2.0 / SQRTPI * x * exp(-x * x));
	}

	return SQRT2 * sigma * zbrent(maxwell, 0.0, x_max, 1.0e-4);
}

double gas_soundspeed(double r, double z, double Mg, double rg, double zg, double Md, double rd, double zd) {
	double cs_gas = 2. * PI * G * exponential_disc_surface_density(r, Mg, rg) * zg;

#ifdef SAMEDISC
	cs_gas *= (1. + Md / Mg);
#else
	if (z != 0.0) {
		cs_gas *= (1. + (exponential_disc_surface_density(r, Md, rd) * tanh(z/zd)) /
						(exponential_disc_surface_density(r, Mg, rg) * tanh(z/zg)));
	}
	else {
		cs_gas *= (1. + exponential_disc_surface_density(r, Md, rd) / exponential_disc_surface_density(r, Mg, rg));
	}
#endif

	return sqrt(cs_gas);
}

double exponential_disc_vertical_dispersion(double R, double Mg, double rg, double zg, double Md, double rd, double zd) {

	double sigmaz =  0.5 * (G * Md / rd) * exp(-R/rd) * (zd / rd);

#ifdef SAMEDISC
	sigmaz *= (1. + Mg / Md);
#else
	sigmaz *= (1. +  exponential_disc_surface_density(R, Mg, rg) / exponential_disc_surface_density(R, Md, rd));
#endif

	return sqrt(sigmaz);
}

#include "allinc.h"

void init_model(GALMOD *sim) {

    double total_mem;
    int i;

    /* INITIALISE THE RANDOM GENERATOR */
	init_random_seed(sim);

    /* INITILISE THE PARTICLE NUMBERS */
	sim->NTot = sim->NGas + sim->NHalo + sim->NDisc + sim->NBulge;
	sim->NCol = sim->NTot - sim->NGas;

#ifdef VERBOSE
    fprintf(stdout, ">>> PARAMETERS DEFINED IN 'allvar.h'\n");
    fprintf(stdout, "--- H0       = %.6lf km/s/Mpc\n", 1e3*H0);
    fprintf(stdout, "--- Q_toomre = %.6lf\n", Q_toomre);
    fprintf(stdout, "--- GAMMA    = %.6lf\n\n", GAMMA);
    fprintf(stdout, ">>> UNITS\n");
    fprintf(stdout, "--- [LENGTH]   = 1 kpc\n");
    fprintf(stdout, "--- [MASS]     = 2.3262e5 Msol\n");
    fprintf(stdout, "--- [VELOCITY] = 1 km/s\n");
    fprintf(stdout, "--- [TIME]     = 0.978 Gyr\n");
    fprintf(stdout, "--- IN THIS UNITS, G = 1\n\n");
#endif

	/* CALCULATE NFW HALO PARAMETERS */
	sim->MHalo = pow(sim->v200, 3) / (10.0 * G * H0);
	sim->r200 = sim->v200 / (10.0 * H0);
	sim->a_halo = sim->r200 / sim->c_halo;

#ifdef VERBOSE
    fprintf(stdout, ">>> NFW HALO PROPERTIES\n");
    fprintf(stdout, "--- M200         = %.3e Msol\n", sim->MHalo * mass_in_solar_masses);
    fprintf(stdout, "--- R200         = %.2lf kpc\n", sim->r200);
    fprintf(stdout, "--- scale radius = %.2lf kpc\n\n", sim->a_halo);
#endif

    /* NORMALISE MASSES */
	sim->MGas *= 1.e10 / mass_in_solar_masses;
	sim->MDisc *= 1.e10 / mass_in_solar_masses;
	sim->MBulge *= 1.e10 / mass_in_solar_masses;

	/* SET PARTICLE MASSES */
    sim->mpGas = (sim->NGas > 0) ? sim->MGas / sim->NGas : 0.0;
    sim->mpHalo = (sim->NHalo > 0) ? sim->MHalo / sim->NHalo : 0.0;
    sim->mpDisc = (sim->NDisc > 0) ? sim->MDisc / sim->NDisc : 0.0;
    sim->mpBulge = (sim->NBulge > 0) ? sim->MBulge / sim->NBulge : 0.0;

#ifdef VERBOSE
    fprintf(stdout, ">>> PARTICLE MASSES\n");
    if (sim->NGas > 0)   fprintf(stdout, "--- gas          = %.3e Msol\n", sim->mpGas * mass_in_solar_masses);
    if (sim->NHalo > 0)  fprintf(stdout, "--- halo         = %.3e Msol\n", sim->mpHalo * mass_in_solar_masses);
    if (sim->NDisc > 0)  fprintf(stdout, "--- stellar disc = %.3e Msol\n", sim->mpDisc * mass_in_solar_masses);
    if (sim->NBulge > 0) fprintf(stdout, "--- bulge        = %.3e Msol\n\n", sim->mpBulge * mass_in_solar_masses);
#endif

#ifdef SAMEDISC
    if (sim->NGas>0) {
		if (sim->R_gas_out != sim->R_disc_out) {
			sim->R_gas_out = sim->R_disc_out;
			fprintf(stderr, "WARNING: outer radius of the gas disc set equal to the stellar one\n");
		}
		if (sim->h_gas != sim->h_disc) {
			sim->h_gas = sim->h_disc;
			fprintf(stderr, "WARNING: scale radius of the gas disc set equal to the stellar one\n");
		}
		if (sim->z0_gas != sim->z0_disc) {
			sim->z0_gas = sim->z0_disc;
			fprintf(stderr, "WARNING: vertical scaleheight of the gas disc set equal to the stellar one\n");
		}
	}
#endif

#ifdef TIPSY
    if((sim->gas_soft == 0.0) && (sim->NGas > 0)) {
        fprintf(stderr, "ERROR: gas softening not specified!\n");
        exit(EXIT_FAILURE);
    }
    if((sim->halo_soft == 0.0) && (sim->NHalo > 0)) {
        fprintf(stderr, "ERROR: halo softening not specified!\n");
        exit(EXIT_FAILURE);
    }
    if((sim->disc_soft == 0.0) && (sim->NDisc > 0)) {
        fprintf(stderr, "ERROR: disc softening not specified!\n");
        exit(EXIT_FAILURE);
    }
    if((sim->bulge_soft == 0.0) && (sim->NBulge > 0)) {
        fprintf(stderr, "ERROR: bulge softening not specified!\n");
        exit(EXIT_FAILURE);
    }
#endif

#ifdef AREPO
    if(sim->box_size == 0.0) {
        sim->box_size = 2. * sim->r200;
        fprintf(stderr, "WARNING: box size set to 2 x Rvir!\n");
    }
#endif

	/* ALLOCATE THE ARRAY FOR PARTICLES */
    if (sim->NTot > 0) {
        sim->p = (PART *)malloc(sim->NTot * sizeof(PART));
        total_mem = sim->NTot * sizeof(PART) / 1024. / 1024.;

#ifdef WITHOMP
        #pragma omp parallel default(shared) private(i)
        {
            int start, end;

            /* FIRST LOOP: GAS */
            start = 0;
            end = sim->NGas;
            #pragma omp for schedule(static)
            for (i=start; i<end; ++i) sim->p[i].internal_energy = 0.0;

            /* SECOND LOOP: HALO */
            start = sim->NGas;
            end = sim->NGas + sim->NHalo;
            #pragma omp for schedule(static)
            for (i=start; i<end; ++i) sim->p[i].internal_energy = 0.0;

            /* THIRD LOOP: DISC */
            start = sim->NGas + sim->NHalo;
            end = sim->NGas + sim->NHalo + sim->NDisc;
            #pragma omp for schedule(static)
            for (i=start; i<end; ++i) sim->p[i].internal_energy = 0.0;

            /* FOURTH LOOP: BULGE */
            start = sim->NGas + sim->NHalo + sim->NDisc;
            end = sim->NTot;
            #pragma omp for schedule(static)
            for (i=start; i<end; ++i) sim->p[i].internal_energy = 0.0;
        }
#else
        for (i=0; i<sim->NTot; ++i) sim->p[i].internal_energy = 0.0;
#endif
    }
    else {
        fprintf(stderr, "ERROR: the total number of particles %d is not valid!\n\n", sim->NTot);
        exit(EXIT_FAILURE);
    }

#ifdef VERBOSE
    fprintf(stdout, ">>> MEMORY ALLOCATION = %.2lf MB\n\n", total_mem);
#endif

	return;
}

void free_memory(GALMOD *sim) {

    /* FREE PARTICLE ARRAY */
    if (sim->NTot > 0) free(sim->p);

    /* FREE RNGs */
#ifdef WITHOMP
    #pragma omp parallel
    {
        int i = omp_get_thread_num();
#pragma omp critical
        {
            gsl_rng_free(sim->rng[i]);
        }
    }
    free(sim->rng);
#else
    gsl_rng_free(sim->rng);
#endif

	fprintf(stdout, ">>> MEMORY ERASED SUCCESFULLY!\n\n");

	return;
}

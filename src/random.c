#include "allinc.h"

void init_random_seed(GALMOD *sim) {

#ifdef WITHOMP
	sim->rng = (gsl_rng **)malloc(sim->NThreads * sizeof(gsl_rng *));

#pragma omp parallel default(shared)
	{
		int nid = omp_get_thread_num();
#pragma omp critical
		{
			sim->rng[nid] = gsl_rng_alloc(gsl_rng_mt19937);
		}
		if (sim->rng[nid] == NULL) {
			fprintf(stderr, "ERROR: not enough memory to create the random number generator on Thread %d!\n\n", nid);
			exit(EXIT_FAILURE);
		}
		gsl_rng_set(sim->rng[nid], sim->random_seed+nid);
	}
#else
	sim->rng = gsl_rng_alloc(gsl_rng_mt19937);
	if (sim->rng == NULL) {
		fprintf(stderr, "ERROR: not enough memory to create the random number generator!\n\n");
		exit(EXIT_FAILURE);
	}
	gsl_rng_set(sim->rng, sim->random_seed);
#endif
	return;
}





























/*
 * This function initialize
 * the random generator setting
 * idum to the user-specified
 * value in the parameter file.
 */

/*
void init_random_seed(long seed)
{
	idum = (long *)malloc(sizeof(long));
	*(idum) = seed;
	return;
}
*/

/*
 * This functions produces a uniform
 * random distribution in the interval [0,1].
 * This routine is from:
 *-----------------------------------------------
 * Numerical Recipes in C, Second Edition (1992)
 *-----------------------------------------------
 */

/*
float ran2(long *idum)
{

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

	int j;
	long k;
	static long idum2=123456789;
	static long iy=0;
	static long iv[NTAB];
	float temp;

	if (*idum <= 0) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		idum2=(*idum);
		for (j=NTAB+7; j>=0; j--) {
			k=(*idum)/IQ1;
			*idum=IA1*(*idum-k*IQ1)-k*IR1;
			if (*idum < 0) *idum += IM1;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ1;
	*idum=IA1*(*idum-k*IQ1)-k*IR1;
	if (*idum < 0) *idum += IM1;
	k=idum2/IQ2;
	idum2=IA2*(idum2-k*IQ2)-k*IR2;
	if (idum2 < 0) idum2 += IM2;
	j=iy/NDIV;
	iy=iv[j]-idum2;
	iv[j] = *idum;
	if (iy < 1) iy += IMM1;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}
*/

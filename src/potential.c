#include "allinc.h"

double halo_potential(double R, double z, double V, double rs, double c) {
	double r = sqrt(R*R + z*z);
	double gc = c/(log(1. + c) - c / (1. + c));
	return -pow(V, 2) * gc * log(1. + r/rs) / (r/rs);
}

double bulge_potential(double R, double z, double M, double a) {
	double r = sqrt(R*R + z*z);
	return - G * M / (r + a);
}

double exponential_disc_potential(double R, double z, double M, double rd, double zd) {
	double y = 0.5 * R / rd;
	double phi = - 0.5 * G * M / rd * y * (gsl_sf_bessel_I0(y) * gsl_sf_bessel_K1(y) - gsl_sf_bessel_I1(y) * gsl_sf_bessel_K0(y));
	return phi + G * M * zd /pow(rd, 2) * exp(-R/rd) * log(cosh(z/zd));
}

double disc_sph_potential_radgrad(double r, double M, double a) {
	return G * M * (1. - (1. + r/a) * exp(-r/a)) / r / r;
}

double halo_potential_radgrad(double r, double V, double rs, double c) {
	double gc = c/(log(1. + c) - c / (1. + c));
	return pow(V, 2) * gc / rs * (log(1. + r / rs) / pow(r / rs, 2) - 1. / (1. + r / rs) / (r / rs));
}

double bulge_potential_radgrad(double r, double M, double a) {
	return G * M / pow(a,2) / pow(1. + r/a, 2);
}

double exponential_disc_vc_squared(double R, double z, double M, double rd) {
	double y;
	y = 0.5 * R / rd;
	return G * M / rd * (y*y*(gsl_sf_bessel_I0(y)*gsl_sf_bessel_K0(y)-gsl_sf_bessel_I1(y)*gsl_sf_bessel_K1(y)));
}

double bulge_vc_squared(double R, double z, double M, double a) {
	double r = sqrt(R*R + z*z);
	return G * M / pow(a, 2) / pow(1. + r / a, 2) * R * R / r;
}

double halo_vc_squared(double R, double z, double V, double rs, double c) {
	double gc = c/(log(1. + c) - c / (1. + c));
	double r = sqrt(R*R + z*z);
	return pow(V, 2) * gc / rs * (log(1. + r / rs) / pow(r / rs, 2) - 1. / (1. + r / rs) / (r / rs)) * R * R / r;
}

double exponential_disc_omega2_ang(double R, double z, double M, double rd) {
	double vc2 = exponential_disc_vc_squared(R, z, M, rd);
	return vc2 / R / R;
}

double bulge_omega2_ang(double R, double z, double M, double a) {
	double vc2 = bulge_vc_squared(R, z, M, a);
	return vc2 / R / R;
}

double halo_omega2_ang(double R, double z, double V, double rs, double c) {
	double vc2 = halo_vc_squared(R, z, V, rs, c);
	return vc2 / R / R;
}

double exponential_disc_kappa2_ang(double R, double z, double M, double rd) {
	double y = 0.5 * R / rd;
	double kappa2 = 0.5 * G * M / pow(rd, 3) * (2. * gsl_sf_bessel_I0(y)*gsl_sf_bessel_K0(y) -
												gsl_sf_bessel_I1(y)*gsl_sf_bessel_K1(y) +
												y * ( gsl_sf_bessel_I1(y)*gsl_sf_bessel_K0(y) -
														gsl_sf_bessel_I0(y)*gsl_sf_bessel_K1(y) ) );
	return kappa2;
}

double bulge_kappa2_ang(double R, double z, double M, double a) {
	double r = sqrt(R*R + z*z);
	return G * M * ( 4. - (1. + 2. * r / (r + a) ) * pow(R/r, 2) ) / r / pow(r + a, 2);
}

double halo_kappa2_ang(double R, double z, double V, double rs, double c) {
	double gc = c/(log(1. + c) - c / (1. + c));
	double r = sqrt(R*R + z*z);

	double kappa2 = pow(V, 2) * gc / pow(rs, 2) * (R/r) * (
		(rs / R - R * rs / r / r) * (log(1. + r/rs) / pow(r/rs, 2) - 1. / (r/rs) / (1. + r/rs)) +
		(R/r) * ( (2. + 3. * (r/rs))/pow(r/rs, 2) / pow(1. + r/rs, 2) - 2. * log(1. + r/rs) / pow(r/rs, 3) )
	);

	kappa2 += 3. * halo_omega2_ang(R, z, V, rs, c);

	return kappa2;
}

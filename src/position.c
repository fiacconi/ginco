#include "allinc.h"

double zbrent(double (*func)(double), double x1, double x2, double tol) {
#define ITMAX 100
#define EPS 3.0e-8
	int iter;
	double a=x1,b=x2,c=x2,d,e,min1,min2;
	double fa=(*func)(a),fb=(*func)(b),fc,p,q,r,s,tol1,xm;

	if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) {
		fprintf(stderr, "\nERROR: Root must be bracketed in zbrent!\n\n");
		exit(EXIT_SUCCESS);
	}
	fc=fb;
	for (iter=1; iter<=ITMAX; iter++) {
		if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
			c=a;
			fc=fa;
			e=d=b-a;
		}
		if (fabs(fc) < fabs(fb)) {
			a=b;
			b=c;
			c=a;
			fa=fb;
			fb=fc;
			fc=fa;
		}
		tol1=2.0*EPS*fabs(b)+0.5*tol;
		xm=0.5*(c-b);
		if (fabs(xm) <= tol1 || fb == 0.0) return b;
		if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
			s=fb/fa;
			if (a == c) {
				p=2.0*xm*s;
				q=1.0-s;
			} else {
				q=fa/fc;
				r=fb/fc;
				p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q=(q-1.0)*(r-1.0)*(s-1.0);
			}
			if (p > 0.0) q = -q;
			p=fabs(p);
			min1=3.0*xm*q-fabs(tol1*q);
			min2=fabs(e*q);
			if (2.0*p < (min1 < min2 ? min1 : min2)) {
				e=d;
				d=p/q;
			} else {
				d=xm;
				e=d;
			}
		} else {
			d=xm;
			e=d;
		}
		a=b;
		fa=fb;
		if (fabs(d) > tol1) {
			b += d;
		} else {
			b += SIGN(tol1,xm);
		}
		fb=(*func)(b);
	}
	fprintf(stderr, "\nERROR: Maximum number of iterations exceeded in zbrent!\n\n");
	exit(EXIT_FAILURE);
	return 0.0;
}

double colatitude_angle(gsl_rng *rng) {
	return acos(1.0 - 2.0 * gsl_rng_uniform(rng));
}

double azimuthal_angle(gsl_rng *rng) {
	return 2.0 * PI * gsl_rng_uniform(rng);
}

double exponential_disc_vertical(gsl_rng *rng) {
	double x = 2.0 * gsl_rng_uniform(rng) - 1.0;
	return 0.5 * log((1.0 + x) / (1.0 - x));
}

double bulge_sferical_radius(const double rs, const double rout, gsl_rng *rng) {
	double A = pow(rout / rs / (1.0 + rout / rs), 2);
	double x = gsl_rng_uniform(rng);
	return sqrt(A * x) / (1.0 - sqrt(x * A));
}

double halo_sferical_radius(const double c, gsl_rng *rng) {

	double yprob = (log(1.0 + c) - c / (1.0 + c)) * gsl_rng_uniform(rng);

	double halo_dfs(double x) {
		return yprob - (log(1.0 + x) - x / (1.0 + x));
	}

	return zbrent(halo_dfs, 0.0, c, 1.0e-5);
}

double exponential_disc_radius(const double rout, const double h, gsl_rng *rng) {

	double yprob = (1.0 - (1.0 + rout/h) * exp(-rout/h)) * gsl_rng_uniform(rng);

	double disc_dfs(double x) {
		return log((1. - yprob) / (1. + x)) + x;
	}

	return zbrent(disc_dfs, 0.0, rout/h, 1.0e-5);
}

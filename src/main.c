#include "allinc.h"

void banner() {
    fprintf(stdout, "\n\n");
	fprintf(stdout, "  |~|~|~|   |~|  |~|   |~|    |~|~|~|     |~|~|~|    \n");
	fprintf(stdout, " |~|   |~|  |~|  |~~|  |~|   |~|   |~|   |~|   |~|   \n");
	fprintf(stdout, "|~|         |~|  |~|~| |~|  |~|    |~|  |~|     |~|  \n");
	fprintf(stdout, "|~|  |~|~|  |~|  |~||~||~|  |~|         |~|     |~|  \n");
	fprintf(stdout, "|~|    |~|  |~|  |~| |~|~|  |~|    |~|  |~|     |~|  \n");
	fprintf(stdout, " |~|   |~|  |~|  |~|  |~~|   |~|   |~|   |~|   |~|   \n");
	fprintf(stdout, "  |~|~|~|   |~|  |~|   |~|    |~|~|~|     |~|~|~|    \n");

	fprintf(stdout, "\n");
	fprintf(stdout, "VERSION %s\n",VERSION);
	fprintf(stdout, "Davide Fiacconi (fiacconi@ast.cam.ac.uk)\n");
	fprintf(stdout, "\n");
	fprintf(stdout, " >>> GINCO (Galaxy INitial COnditions) sets up the inital <<< \n");
	fprintf(stdout, " >>> conditions for N-body models of disc galaxies.       <<< \n");
	fprintf(stdout, "\n");
    return;
}

int main(int argc, char *argv[]) {

    GALMOD MyModel;

	/* CHECK FOR THE RIGHT NUMBER OF INPUT PARAMENTERS */
	if (argc < 2) {
		fprintf(stderr, "\nERROR: Wrong number of input arguments!\n");
		fprintf(stderr, "Correct usage:\n");
		fprintf(stderr, "\n\t..$ ./GINCO parameterfile [number_OpenMP_threads]\n\n");
		fprintf(stderr, "To get a template of the parameter file, type './GINCO newfile'.\n\n");
		exit(EXIT_FAILURE);
	}

	/* CHECK FOR A NEW TEMPLATE PARAMETER FILE */
	if (!strcmp(argv[1],"newfile")) {
		write_parameterfile_template();
		exit(EXIT_FAILURE);
	}

    /* CHECK FOR THE NUMBER OF THREADS IF OpenMP IS USED */
#ifdef WITHOMP
    if (argc < 3) {
        fprintf(stderr, "ERROR: code compiled with the OpenMP support, but the number\n");
        fprintf(stderr, "of threads has not been specified!\n");
        exit(EXIT_FAILURE);
    }
    MyModel.NThreads = (int)atoi(argv[2]);
    omp_set_num_threads(MyModel.NThreads);
#endif


    banner();                                   /* PRINT THE BANNER */

	read_parameterfile(argv[1], &MyModel);      /* READ PARAMETER FILE */

	init_model(&MyModel);                       /* INITIALISE THE MODEL */

	make_positions(&MyModel);                   /* SET PARTICLE POSITIONS */

	make_velocities(&MyModel);                  /* SET PARTICLE VELOCITIES */

	write_snapshot(&MyModel);                   /* WRITE IC FILE */

	free_memory(&MyModel);                      /* FREE ALLOCATED MEMORY */

	exit(EXIT_SUCCESS);
}

#ifndef __ALLINC_H__
#define __ALLINC_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#ifdef WITHOMP
#include <omp.h>
#endif

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

#define VERSION "1.0.0"

#define MAXCHAR 240

#define PI 3.141592653589793
#define SQRT2 1.414213562373095
#define SQRT3 1.732050807568877
#define SQRTPI 1.772453850905516

#define GAMMA (5.0/3.0)
#define Q_toomre 1.5

#define G 1.000
#define H0 0.0702
#define BOLTZMANN 1.3806505e-16
#define PROTONMASS 1.67262171e-24
#define HYDROGEN_MASSFRAC 0.76

#define mass_in_solar_masses 2.3262e5
#define length_in_cm 3.085677580666e+21
#define velocity_in_cgs 1.000e+5

typedef struct particle_data {
	float  pos[3];
	float  vel[3];
    float  internal_energy;
} PART;

typedef struct galaxy_model {

#ifdef WITHOMP
    int NThreads;
#endif

	unsigned long int random_seed;

#ifdef WITHOMP
    gsl_rng **rng;
#else
    gsl_rng *rng;
#endif

    char parameter_file_name[MAXCHAR];

	unsigned int NTot;
    unsigned int NCol;
    unsigned int NGas;
    unsigned int NHalo;
    unsigned int NDisc;
    unsigned int NBulge;

    double MGas;
    double MHalo;
    double MDisc;
    double MBulge;

    double mpGas;
    double mpHalo;
    double mpDisc;
    double mpBulge;

    double R_gas_out;
    double h_gas;
    double z0_gas;
	double turb_gas;

    double v200;
    double r200;
    double a_halo;
    double c_halo;

    double R_disc_out;
    double h_disc;
    double z0_disc;

    double R_bulge_out;
    double a_bulge;

#ifdef TIPSY
	double gas_soft;
	double halo_soft;
	double disc_soft;
	double bulge_soft;
#endif

#ifdef AREPO
	double box_size;
#endif

    PART *p;
} GALMOD;

/* FROM density.c */
double exponential_disc_density(double r, double z, double M, double rd, double zd);
double exponential_disc_surface_density(double r, double M, double rd);
double bulge_density(double r, double M, double a);
double halo_density(double r, double M, double rs, double c);

/* FROM galaxy.c */
void make_positions(GALMOD *sim);
void make_velocities(GALMOD *sim);

/* FROM init.c */
void init_model(GALMOD *sim);
void free_memory(GALMOD *sim);

/* FROM io.c */
void read_parameterfile(char *filename, GALMOD *sim);
void write_parameterfile_template();
void write_snapshot(GALMOD *sim);

/* FROM position.c */
double zbrent(double (*func)(double), double x1, double x2, double tol);
double colatitude_angle(gsl_rng *rng);
double azimuthal_angle(gsl_rng *rng);
double exponential_disc_vertical(gsl_rng *rng);
double bulge_sferical_radius(const double rs, const double rout, gsl_rng *rng);
double halo_sferical_radius(const double c, gsl_rng *rng);
double exponential_disc_radius(const double rout, const double h, gsl_rng *rng);

/* FROM potential.c */
double halo_potential(double R, double z, double V, double rs, double c);
double bulge_potential(double R, double z, double M, double a);
double exponential_disc_potential(double R, double z, double M, double rd, double zd);
double disc_sph_potential_radgrad(double r, double M, double a);
double halo_potential_radgrad(double r, double V, double rs, double c);
double bulge_potential_radgrad(double r, double M, double a);
double exponential_disc_vc_squared(double R, double z, double M, double rd);
double bulge_vc_squared(double R, double z, double M, double a);
double halo_vc_squared(double R, double z, double V, double rs, double c);
double exponential_disc_omega2_ang(double R, double z, double M, double rd);
double bulge_omega2_ang(double R, double z, double M, double a);
double halo_omega2_ang(double R, double z, double V, double rs, double c);
double exponential_disc_kappa2_ang(double R, double z, double M, double rd);
double bulge_kappa2_ang(double R, double z, double M, double a);
double halo_kappa2_ang(double R, double z, double V, double rs, double c);

/* FROM random.c */
void init_random_seed(GALMOD *sim);

/* FROM velocity.c */
double gas_soundspeed(double r, double z, double Mg, double rg, double zg, double Md, double rd, double zd);
double bulge_dispersion_velocity(double, double, double, double, double, double, double, double, double, double);
double halo_dispersion_velocity(double, double, double, double, double, double, double, double, double, double);
double maxwellian_velocity_distribution(gsl_rng *rng, double sigma, double vesc);
double exponential_disc_vertical_dispersion(double R, double Mg, double rg, double zg, double Md, double rd, double zd);

/* FROM io_routines/io_ascii.c */
#ifdef ASCII
void write_ic_ascii(GALMOD *sim);
#endif

/* FROM io_routines/io_gadget.c */
#if defined(GADGET2) || defined(AREPO)
void write_ic_gadget(GALMOD *sim);
#endif

/* FROM io_routines/io_tipsy.c */
#ifdef TIPSY
void write_ic_tipsy(GALMOD *sim);
#endif

#endif
